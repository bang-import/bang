from bang import Card

default_deck = [
	{ 'name' : 'BANG!', 'kind' : 'bang', 'action' : [Card.hit] },
	{ 'name' : 'Knife', 'kind' : 'hit', 'action' : [Card.hit] },
	{ 'name' : 'Missed!', 'kind' : 'miss', 'action': [Card.missed] },
	{ 'name' : 'Beer', 'kind' : 'beer', 'action' : [Card.recover_life] },
	{ 'name' : 'Whisky', 'kind' : 'whisky', 'action' : [Card.recover_life, Card.recover_life] }
	]
