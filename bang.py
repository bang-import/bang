import random

class Player:
	def __init__ (self, name, role):
		self.name    = "Pedro"
		self.role    = "Outlaw"
		self.max_hitpoints = 4
		self.hitpoints = 4
		self.character = "Santa Claus"
		self.max_bangs = 1
		self.round_bangs = 0
		self.hand = []
		self.table = []

	def draw(self, number, deck):
		for n in range(0,number):
			deck[0].owner = self
			self.hand.append(deck[0])
			deck.pop(0)


	def playCard(self, card, target=None):
		if card in self.hand:
			for action in card.actions:
				use = card.use(self, action, target)	
			if use:
				print self.name + " played " + card.name
				self.discardCard(card)
				return use
			else:
				print 'Card ' + card.name + ' cannot be played'
	def discardCard(self, card):
		if card in self.hand:
			card.discard(card)

class Card:
	def __init__(self, name, kind, actions, game):
		self.name = name
		self.kind = kind
		self.actions = actions
		self.type = 'Effect'
		self.image = ''
		self.suit = 'Hearts'
		self.number = '9'
		self.owner = 'Deck'
		self.game = game

	def use(self, owner, action, target=None):
		for t in target:
			result = action(self, owner, t)
			return result

	#define actions
	def hit(self, owner, target):
		if self.kind == 'bang' and owner.round_bangs < owner.max_bangs:
			print owner.name + ' is shooting ' + target.name
			owner.round_bangs = owner.round_bangs + 1
			results = self.game.turn.waitforCard(owner, target, ['miss'])
			if not results:			
				target.hitpoints = target.hitpoints - 1
			else:			
				return 1
		elif self.kind == 'bang' and owner.round_bangs == owner.max_bangs:
			return 0
		else:
			print owner.name + ' is shooting ' + target.name
			results = self.game.turn.waitforCard(owner, target, ['miss'])
			if not results:
				target.hitpoints = target.hitpoints - 1
			else:
				return 1
		if target.hitpoints == 0:
			results = self.game.turn.waitforCard(owner, target, ['beer'])
			if not results:
				print target.name + " is dead"
				self.game.player.remove(target)
		return 1
	
	def recover_life(self, owner, target):
		if target.hitpoints < target.max_hitpoints:
			print target.name + ' is drinking a beer'
			target.hitpoints = target.hitpoints + 1
			return 1

	def missed(self, owner, target):
		print 'Missed!'
		return 1

	def draw_card(self, owner, target):
		target.draw(1, self.game.deck)

	def discard(self, card):
		print 'Discarded ' + card.name
		self.game.trash.append(card)
		self.owner.hand.remove(card)
class Game:
	def shuffleDeck(self):
		random.shuffle(self.deck)

	def set_players(self, num_players):
		players = []
		for p in range(0,num_players):
			players.append(Player('Pedro', 'Outlaw'))
		for p in players:
			p.draw(5, self.deck)
		self.player = players

	def load_deck(self, cards):
		deck = []
		for card in cards:
			deck.append(Card(card['name'],card['kind'],card['action'],self))
		self.deck = deck
		self.shuffleDeck()

	def start_turn(self, player):
		self.turn = Turn(self, player)
		self.turn.phase1()
		while True:
			if self.turn.phase2(): break
		self.turn.discardPhase()

	def __init__(self):
		self.player = []
		self.deck = []
		self.trash = []

class Turn:
	def __init__(self, game, player):
		self.player = player
		self.game = game

	def phase1(self):
		self.player.round_bangs = 0
		self.player.draw(2, self.game.deck)

	def phase2(self):
		print self.player.name + "'s turn"
		print "You've " + str(self.player.hitpoints) + " hitpoints left"
		print "Your hand:"
		card_count = 0
		for card in self.player.hand:
			print '[' + str(card_count) + '] Card:' + card.name
			card_count = card_count + 1
		print '['+ str(card_count+1) + '] End Turn'
		print 'Total: ' + str(card_count)
	
		#tosco system		
		print "Choose a card:"
		c_card = raw_input('--> ')
		if c_card == str(card_count+1): return 1

		print self.player.hand[int(c_card)].name
		print "Choose a target"
		p_count = 0
		for p in self.game.player:
			print '[' + str(p_count) + '] ' + p.name
			p_count = p_count + 1
		c_target = raw_input('--> ')
		print 'Target: ' + self.game.player[int(c_target)].name
		self.player.playCard(self.player.hand[int(c_card)], [self.game.player[int(c_target)]])
		#end tosco system

	def discardPhase(self):
		while len(self.player.hand)-1 > self.player.hitpoints:
			print self.player.name + " must discard " + str(len(self.player.hand)-1-self.player.hitpoints) + " cards"
			print "Your hand:"
			card_count = 0
			for card in self.player.hand:
				print '[' + str(card_count) + '] Card:' + card.name
				card_count = card_count + 1
			print 'Total: ' + str(card_count)
	
			#tosco system		
			print "Choose a card:"
			c_card = raw_input('--> ')
			print self.player.hand[int(c_card)].name
			self.player.discardCard(self.player.hand[int(c_card)])
			#end tosco system

	def waitforCard(self,owner, target, valid_cards=None):
		print target.name + " answer:"
		print "You've " + str(target.hitpoints) + " hitpoints left"
		print "Your hand:"
		card_count = 0
		for card in target.hand:
			if card.kind in valid_cards: print '[' + str(card_count) + '] Card:' + card.name
			card_count = card_count + 1
		print '['+ str(card_count+1) + '] Do Nothing'			
		print 'Total: ' + str(card_count)
	
		#tosco system		
		print "Choose a card:"
		c_card = raw_input('--> ')
		if c_card == str(card_count+1): return 0

		print target.hand[int(c_card)].name
		return target.playCard(target.hand[int(c_card)], [owner])
		#end tosco system


